<?php
/**
 * VW Restaurant Lite Theme Customizer
 *
 * @package VW Restaurant Lite
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function vw_restaurant_lite_customize_register( $wp_customize ) {	

	//add home page setting pannel
	$wp_customize->add_panel( 'vw_restaurant_lite_panel_id', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'VW Settings', 'vw-restaurant-lite' ),
	    'description' => __( 'Description of what this panel does.', 'vw-restaurant-lite' ),
	) );

	//theme Layouts
	$wp_customize->add_section( 'vw_restaurant_lite_left_right', array(
    	'title'      => __( 'Theme Layout Settings', 'vw-restaurant-lite' ),
		'priority'   => 30,
		'panel' => 'vw_restaurant_lite_panel_id'
	) );

	// Add Settings and Controls for Layout
	$wp_customize->add_setting('vw_restaurant_lite_theme_options',array(
	        'default' => '',
	        'sanitize_callback' => 'vw_restaurant_lite_sanitize_choices'
	    )
    );

	$wp_customize->add_control('vw_restaurant_lite_theme_options',
	    array(
	        'type' => 'radio',
	        'label' => 'Do you want this section',
	        'section' => 'vw_restaurant_lite_left_right',
	        'choices' => array(
	            'Left Sidebar' => __('Left Sidebar','vw-restaurant-lite'),
	            'Right Sidebar' => __('Right Sidebar','vw-restaurant-lite'),
	            'One Column' => __('One Column','vw-restaurant-lite'),
	            'Three Columns' => __('Three Columns','vw-restaurant-lite'),
	            'Four Columns' => __('Four Columns','vw-restaurant-lite'),
	            'Grid Layout' => __('Grid Layout','vw-restaurant-lite')
	        ),
	    )
    );
	
	//home page slider
	$wp_customize->add_section( 'vw_restaurant_lite_slidersettings' , array(
    	'title'      => __( 'Slider Settings', 'vw-restaurant-lite' ),
		'priority'   => 30,
		'panel' => 'vw_restaurant_lite_panel_id'
	) );

	for ( $count = 1; $count <= 4; $count++ ) {

		// Add color scheme setting and control.
		$wp_customize->add_setting( 'vw_restaurant_lite_slidersettings-page-' . $count, array(
				'default'           => '',
				'sanitize_callback' => 'absint'
		) );

		$wp_customize->add_control( 'vw_restaurant_lite_slidersettings-page-' . $count, array(
			'label'    => __( 'Select Slide Image Page', 'vw-restaurant-lite' ),
			'section'  => 'vw_restaurant_lite_slidersettings',
			'type'     => 'dropdown-pages'
		) );

	}

	//Topbar section
	$wp_customize->add_section('vw_restaurant_lite_topbar_icon',array(
		'title'	=> __('Topbar Section','vw-restaurant-lite'),
		'description'	=> __('Add Top Header Content here','vw-restaurant-lite'),
		'priority'	=> null,
		'panel' => 'vw_restaurant_lite_panel_id',
	));

	$wp_customize->add_setting('vw_restaurant_lite_contact',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_contact',array(
		'label'	=> __('Add Phone Number','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_icon',
		'setting'	=> 'vw_restaurant_lite_contact',
		'type'		=> 'text'
	));

	$wp_customize->add_setting('vw_restaurant_lite_email',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_email',array(
		'label'	=> __('Add Email','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_icon',
		'setting'	=> 'vw_restaurant_lite_email',
		'type'		=> 'text'
	));

	//Social Icons(topbar)
	$wp_customize->add_section('vw_restaurant_lite_topbar_header',array(
		'title'	=> __('Social Icon Section','vw-restaurant-lite'),
		'description'	=> __('Add Header Content here','vw-restaurant-lite'),
		'priority'	=> null,
		'panel' => 'vw_restaurant_lite_panel_id',
	));

	$wp_customize->add_setting('vw_restaurant_lite_headertwitter',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_headertwitter',array(
		'label'	=> __('Add twitter link','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_header',
		'setting'	=> 'vw_restaurant_lite_headertwitter',
		'type'		=> 'text'
	));

	$wp_customize->add_setting('vw_restaurant_lite_headerpinterest',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_headerpinterest',array(
		'label'	=> __('Add pinterest link','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_header',
		'setting'	=> 'vw_restaurant_lite_headerpinterest',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('vw_restaurant_lite_headerfacebook',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_headerfacebook',array(
		'label'	=> __('Add facebook link','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_header',
		'setting'	=> 'vw_restaurant_lite_headerfacebook',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('vw_restaurant_lite_headeryoutube',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_headeryoutube',array(
		'label'	=> __('Add Youtube link','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_header',
		'setting'	=> 'vw_restaurant_lite_headeryoutube',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('vw_restaurant_lite_headerinstagram',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_headerinstagram',array(
		'label'	=> __('Add Instagram link','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_topbar_header',
		'setting'	=> 'vw_restaurant_lite_headerinstagram',
		'type'	=> 'text'
	));
	
	//we Belive
	$wp_customize->add_section('vw_restaurant_lite_belive',array(
		'title'	=> __('We Belive Section','vw-restaurant-lite'),
		'description'	=> __('Add We Belive sections below.','vw-restaurant-lite'),
		'panel' => 'vw_restaurant_lite_panel_id',
	));

	$post_list = get_posts();
	$i = 0;
	foreach($post_list as $post){
		$posts[$post->post_title] = $post->post_title;
	}

	$wp_customize->add_setting('vw_restaurant_lite_belive_post_setting',array(
		'sanitize_callback' => 'sanitize_text_field',
	));

	$wp_customize->add_control('vw_restaurant_lite_belive_post_setting',array(
		'type'    => 'select',
		'choices' => $posts,
		'label' => __('Select post','vw-restaurant-lite'),
		'section' => 'vw_restaurant_lite_belive',
	));

	//footer text
	$wp_customize->add_section('vw_restaurant_lite_footer_section',array(
		'title'	=> __('Footer Text','vw-restaurant-lite'),
		'description'	=> __('Add some text for footer like copyright etc.','vw-restaurant-lite'),
		'panel' => 'vw_restaurant_lite_panel_id'
	));
	
	$wp_customize->add_setting('vw_restaurant_lite_footer_copy',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));
	
	$wp_customize->add_control('vw_restaurant_lite_footer_copy',array(
		'label'	=> __('Copyright Text','vw-restaurant-lite'),
		'section'	=> 'vw_restaurant_lite_footer_section',
		'type'		=> 'text'
	));
	
}
add_action( 'customize_register', 'vw_restaurant_lite_customize_register' );	

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function vw_restaurant_lite_customize_preview_js() {
	wp_enqueue_script( 'vw_restaurant_lite_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'vw_restaurant_lite_customize_preview_js' );


/**
 * Singleton class for handling the theme's customizer integration.
 *
 * @since  1.0.0
 * @access public
 */
final class vw_restaurant_lite_customize {

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Sets up initial actions.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup_actions() {

		// Register panels, sections, settings, controls, and partials.
		add_action( 'customize_register', array( $this, 'sections' ) );

		// Register scripts and styles for the controls.
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_control_scripts' ), 0 );
	}

	/**
	 * Sets up the customizer sections.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  object  $manager
	 * @return void
	 */
	public function sections( $manager ) {

		// Load custom sections.
		load_template( trailingslashit( get_template_directory() ) . '/inc/section-pro.php' );

		// Register custom section types.
		$manager->register_section_type( 'vw_restaurant_lite_customize_Section_Pro' );

		// Register sections.
		$manager->add_section(
			new vw_restaurant_lite_customize_Section_Pro(
				$manager,
				'example_1',
				array(
					'title'    => esc_html__( 'VW Restaurant Pro', 'vw-restaurant-lite' ),
					'pro_text' => esc_html__( 'Go Pro',         'vw-restaurant-lite' ),
					'pro_url'  => 'http://www.vwthemes.net/vw-restaurant-theme/'
				)
			)
		);
	}

	/**
	 * Loads theme customizer CSS.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function enqueue_control_scripts() {

		wp_enqueue_script( 'vw-restaurant-lite-customize-controls', trailingslashit( get_template_directory_uri() ) . '/js/customize-controls.js', array( 'customize-controls' ) );

		wp_enqueue_style( 'vw-restaurant-lite-customize-controls', trailingslashit( get_template_directory_uri() ) . '/css/customize-controls.css' );
	}
}

// Doing this customizer thang!
vw_restaurant_lite_customize::get_instance();