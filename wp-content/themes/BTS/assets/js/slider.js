jQuery(document)
	.ready(
		function($)
		{
			//Slider
			var time  = 8000;
			var timed = setTimeout(slideShow, time);

			function slideShow(target, link)
			{
				clearTimeout(timed);

				var target = target;
				var link   = link;

				if(target == undefined && link == undefined)
				{
					if($('.slider-control li:last-child').hasClass('active'))
					{

						target               = $('.slider .slide:first-child');
						link				 = $('.slider-control li:first-child');

					}else{

						target               = $('.slider .slide.active').next();
						link				 = $('.slider-control li.active').next();

					}
				}

				$('.slider .slide').removeClass('active');
				$('.slider-control li').removeClass('active');

				$(target).addClass('active fadeIn');
				$(link).addClass('active');

				timed = setTimeout(slideShow, time);
			}

			$('.slider-control a').click(
				function(e)
				{
					e.preventDefault();

					var target = $(this).attr('href');
					var link   = $(this).parent();

					slideShow(target, link);
				}
			);
	}
);