jQuery(document)
	.ready(
		function($)
		{
			console.log('*** THEME JS INIT ***');

			$('#mobile-button')
				.on('click touchstart',
					function(e)
					{
						e.preventDefault();
						$('#mobile-navigation').toggleClass('active');
					}
				);

			/*
				FORM Handling
			*/
			$('.form')
				.validate({
			  		submitHandler: function(form)
			  		{
			  			var _uri  = $(form).attr('action');

			  			console.log('Processing ..');

						$.post(_uri, $(form).serialize())
							.done(
								function(response)
								{
									if(response.mail_status == true){
										$(form).parent().append('<h2>Thank you for your submission</h2>');
										$(form).fadeOut();
									}
								}
							);

		  			}
			 	});

			$('#client-information')
				.validate({
					submitHandler: function(form)
			  		{
			  			$('#paypal-submit-overlay').hide();
		  			}
				});

			$('#client-information')
				.on('change',
					function()
					{
						$('#client-information').submit();
					});

			$('#paypal-submit-overlay')
				.on('touchend click',
					function()
					{
						$('#client-information').submit();
					});

			 $('.datepicker').pickadate();
			 $('.timepicker').pickatime();


			//Trigger form on anchor buttons
			$('.form .submit')
				.click(
					function(e)
					{
						e.preventDefault();

						var _form = $(this).parent().parent();

						$(_form).submit();
					}
				);

			$('.form-checkout .submit')
				.click(
					function(e)
					{
						e.preventDefault();

						var _form = $(this).parent().parent();

						$(_form).submit();
					}
				);

			/*
				Checkout Process
			*/

			var _menuItems = [];

			function showCheckout()
			{
				$.each(_menuItems,
					function(index, value)
					{
						var name = (value.variation) ? value.name + ' : ' + value.variation : value.name;

						var html = '<tr><td>'+name+'</td><td> $ '+value.price+'</td></tr>';
						$('#checkout-data table tbody').append(html);
					}
				);
			}

			function sendData(){

				var _data          = {};

				_data.id           = $('#orderID').val();
				_data.paymentID    = $('#paymentID').val();
				_data.payerID      = $('#payerID').val();
				_data.items        = _menuItems;
				_data.subtotal     = $('#subtotal-checkout').val();
				_data.charge       = $('#charge-checkout').val();
				_data.total        = $('#total-checkout').val();
				_data.name         = $('#client-information input[name="Name"]').val();
				_data.phone        = $('#client-information input[name="Phone"]').val();
				_data.email        = $('#client-information input[name="Email"]').val();
				_data.table_letter = $('#client-information input[name="TableLetter"]').val();
				_data.table_number = $('#client-information input[name="TableNumber"]').val();
				_data.time_arrival = $('#client-information input[name="TimeofArrival"]').val();

				$.post('/wp-content/themes/BTS/library/forms.php', _data)
					.done(
						function(response)
						{
							console.log(response);
							$('#step-3').removeClass('active');
							$('#step-4').addClass('active');
							$('#step-nav').addClass('hide');
						});

			}

			$('#finalize')
				.click(
					function(e){
						e.preventDefault();
						sendData();
					}
				);

			$('#menu-checkout input[type="checkbox"]')
				.change(
					function(e)
					{
						e.preventDefault();

						var data        = {};
						var _subtotal   = 0;
						var _charge     = 0;
						var _total      = 0;
						var _tax        = 0;
						var _percentage = $('#charge-data').val();
						var _perctax    = $('#tax-data').val();

						data.name      = $(this).attr('name');
						data.price     = $(this).val();

						if($(this).attr('data-variation')){

							data.variation = $(this).attr('data-variation');

						}

						if(this.checked){

							_menuItems.push(data);

						}else{

							for (var i=_menuItems.length-1; i>=0; i--)
							{
							    if (_menuItems[i].name === data.name)
							    {
							        _menuItems.splice(i, 1);
							    }
							}
						}

						$.each(_menuItems,
							function(index, value)
							{
								_subtotal += parseFloat(value.price);
							}
						);

						_charge = _subtotal * (_percentage/100);
						_tax    = _subtotal * (_perctax/100);
						_total  = _subtotal + _charge + _tax;

						$('.getsubtotal').text('$ ' + _subtotal.toFixed(2));
						$('.getcharge').text('$ ' + _charge.toFixed(2));
						$('.gettax').text('$ ' + _tax.toFixed(2));
						$('.gettotal').text('$ ' + _total.toFixed(2));

						$('#step-2 #subtotal-checkout').val(_subtotal.toFixed(2));
						$('#step-2 #charge-checkout').val(_charge.toFixed(2));
						$('#step-2 #tax-checkout').val(_tax.toFixed(2));
						$('#step-2 #total-checkout').val(_total.toFixed(2));

					}
				);


			$('#proceed')
				.click(
					function(e)
					{
						e.preventDefault();

						var _total = $('#step-2 #total-checkout').val();

						if(_total > 0){

							//Goto Step 3
							$('#step-nav li').removeClass('active');
							$('#step-nav li:last-child').addClass('active');

							$('#step-2').addClass('hide');
							$('#step-3').addClass('active');

							showCheckout();

						}else{

							//Print Message that cant process with 0.00 value

						}

					}
				);



			/*
				Special Events Slider
			*/
			var _column;
			var _parentSize;
			var _elements;
			var _state;
			var _curelements;
			var _confcols;

			function setColumns(){

				//Reset
				$('#special-v-5 .columns .col').css('height', 'auto');
				$('#special-v-5 .columns').attr('data-state', 0);
				$('#special-v-5 .columns').css('margin-left', 0);

				var _winWidth = $(window).width();
				var _height   = 0;


				//Responsiveness
				if(_winWidth >= 2210){

					_curelements = 6;
					_confcols    = 6;

				}else if(_winWidth >= 890){

					_curelements = 4;
					_confcols    = 4;

				}else if(_winWidth >= 590){

					_curelements = 2;
					_confcols    = 2;

				}else{

					_curelements = 1;
					_confcols    = 1;

				}

				_curelements;
				_confcols;

				_elements     = $('#special-v-5 .columns .col').size();
				_column       = _winWidth / _confcols;
				_parentSize   = _elements * _column;
				_state        = $('#special-v-5 .columns').attr('data-state');

				_cols = $('#special-v-5 .columns .col');

				$('#special-v-5 .columns .col').css('width', _column);
				$('#special-v-5 .columns').css('width', _parentSize);

				$.each(_cols,
					function(index, value)
					{
						var _colHeight = $(value).height();
						_height = (_colHeight > _height) ? _colHeight : _height;
					}
				);

				$('#special-v-5 .columns .col').css('height', _height);
				$('#special-v-5 .control').css('padding-top', _height/2);

			}

			$('#special-v-5 .control')
				.click(
					function(e)
					{
						e.preventDefault();

						var _direction = $(this).attr('href');

						if(_direction == '#left'){

							$('#right-col').addClass('active');

							if(_curelements < _elements){

								_state--;
								var _left = (_state * _column);

								$('#special-v-5 .columns').css('margin-left', _left);
								$('#special-v-5 .columns').attr('data-state', _state);
								_curelements++;

								if(_curelements == _elements){
									$('#left-col').removeClass('active');
								}

							}

						}else{

							$('#left-col').addClass('active');

							if(_curelements > _confcols){

								_state++;
								var _right = (_state * _column);

								$('#special-v-5 .columns').css('margin-left', _right);
								$('#special-v-5 .columns').attr('data-state', _state);
								_curelements--;

								if(_curelements == _confcols){
									$('#right-col').removeClass('active');
								}

							}

						}

					}
				);

			setColumns();

			$(window)
				.resize(
					function(){
						setColumns();
					}
				);

		}
	);