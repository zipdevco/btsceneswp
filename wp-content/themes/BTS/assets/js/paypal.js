paypal.Button.render({

    env: 'sandbox', // sandbox | production

    // PayPal Client IDs - replace with your own
    // Create a PayPal app: https://developer.paypal.com/developer/applications/create
    client: {
        //Developer Information
        // sandbox:    'ARi-EDiGPz2VPK3P081vvIZdHY0nPhLiiVZ0NkU28eZkuzHPj8pSczFQJ_QAESdKx9yF5IgLHQy9Rxst',
        // production: 'AaGwRhUQHM0JyGkp6qBbokTd8NZzjOnpJcVvkK5-C2cbL_4J1a74RlCln-DT4-EaWFrbORbkUzhrmqLu'
        // Client Information
        sandbox:    'AXpFzbxlNFltS7xJrtEOVamoJ5a3YwBVgbxK35P1XGDRQel79CNPb4NYN_QXr7lVVuXGsM96HYUyxdc',
        production: 'AVO-b2hSS4WaDZ1xOuj8IdJZoEKKUYd1q1-SICzUfPnRKmv5BMO_0Y91tlJX1O8JCLorYceEMTX4CSpv'
    },

    commit: true, // Show a 'Pay Now' button

    style: {
        label: 'checkout', // checkout | credit | pay
        size:  'medium',    // small | medium | responsive
        shape: 'rect',     // pill | rect
        color: 'blue'      // gold | blue | silver
    },

    // payment() is called when the button is clicked
    payment: function(data, actions) {

        var _total = jQuery('#total-checkout').val();

        // Make a call to the REST api to create the payment
        return actions.payment.create({
            transactions: [
                {
                    amount: { total: _total, currency: 'USD' }
                }
            ]
        });
    },

    // onAuthorize() is called when the buyer approves the payment
    onAuthorize: function(data, actions) {

        // Make a call to the REST api to execute the payment
        return actions.payment.execute().then(function() {

            jQuery('#payerID').val(data.payerID);
            jQuery('#paymentID').val(data.paymentID);
            jQuery('#finalize').trigger("click");

        });
    }

}, '#paypal-button-container');