<?php if( have_rows('he_slider') ):	?>

<section id="hero-section">

	<div class="slider">

		<?php

			$count = 1;

			while ( have_rows('he_slider') ) : the_row();

			$active = ($count == 1) ? 'active' : '';
		?>

			<div id="slider-<?php echo $count; ?>" class="slide animated <?php echo $active; ?>">
				<div class="content" style="background-image:url('<?php the_sub_field('bg_image'); ?>');">
					<div class="aligned">
						<?php the_sub_field('slider_content'); ?>
					</div>
				</div>
			</div>

		<?php
			$count++;

			endwhile;
		?>

		<ul class="slider-control">

			<?php

				$count = 1;

				while ( have_rows('he_slider') ) : the_row();

				$active = ($count == 1) ? 'active' : '';
			?>

				<li class="<?php echo $active; ?>"><a href="#slider-<?php echo $count; ?>"></a></li>

			<?php
				$count++;

				endwhile;
			?>

		</ul>

	</div>

</section>
<!-- end hero section -->

<?php endif; ?>