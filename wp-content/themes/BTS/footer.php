<footer id="footer">
	<div class="wrapper group">
		<div class="col-left group">
			<span class="title">
				Summary
			</span>
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '') ); ?>
		</div>
		<div class="col-mid group">
			<a href="#">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bts-footer-logo.png" alt="">
			</a>
			<p>
				2006-2017 Behind the Scenes Catering and Events
			</p>
		</div>
		<div class="col-right group">
			<span class="title">
				Contact Us
			</span>
			<p>
				9888 Waples Street, San Diego, CA 92121<br>
				858 638 1400
			</p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>