<?php
/*
	Template Name: Apply Now Form
*/
get_header();
?>
<div id="apply-now" class="bg-gray">
	<div class="wrapper bg-white txt-center">
		<form action="" class="form">
			<div class="header-content">
				<?php the_content(); ?>
			</div>
			<div class="single-input">
				<input type="text" placeholder="First Name *">
			</div>
			<div class="single-input">
				<input type="text" placeholder="Last Name *">
			</div>
			<div class="single-input">
				<input type="text" placeholder="Email *">
			</div>
			<div class="single-input">
				<input type="text" placeholder="Phone Number *">
			</div>
			<div class="single-input">
				<input type="text" placeholder="Position you are applying for *">
			</div>
			<div class="two-columns">
				<div class="col txt-left">
					<label for="">What is your availability? *</label>
				</div>
				<div class="col">
					<div class="input-icon">
						<input type="text" class="input" placeholder="Pick a possible interview date">
						<a class="icon" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-date.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="singe-input txt-left">
				<label>Please List (2) Professional References *</label>
			</div>
			<div class="group">
				<div class="single-input">
					<input type="text" placeholder="Full Name *">
				</div>
				<div class="single-input">
					<input type="text" placeholder="Phone Number *">
				</div>
				<div class="single-input">
					<input type="text" placeholder="Email *">
				</div>
				<div class="single-input">
					<input type="text" placeholder="Reference Relationship *">
				</div>
			</div>
			<div class="group">
				<div class="single-input">
					<input type="text" placeholder="Full Name *">
				</div>
				<div class="single-input">
					<input type="text" placeholder="Phone Number *">
				</div>
				<div class="single-input">
					<input type="text" placeholder="Email *">
				</div>
				<div class="single-input">
					<input type="text" placeholder="Reference Relationship *">
				</div>
			</div>
			<div class="two-columns txt-left">
				<div class="col">
					<label id="referred" for="">Let us know if you were referred to us by someone *</label>
				</div>
				<div class="col txt-left">
					<a class="radio active" href="#">
						<span class="container"><input type="radio" name="set1" /></span>
						<span class="text">Yes</span>
					</a>
					<a class="radio" href="#">
						<span class="container"><input type="radio" name="set1" /></span>
						<span class="text">No</span>
					</a>
				</div>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Full Name *">
			</div>
			<div class="single-input txt-center">
				<a href="#" class="btn brown text-center">ADD RESUME</a>
			</div>
			<div class="single-input txt-center">
				<a href="#" class="btn purple text-center">SEND</a>
			</div>
		</form>
	</div>
</div>
<?php get_footer(); ?>