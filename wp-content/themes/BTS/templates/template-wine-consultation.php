<?php
/*
	Template Name: Wine Consultation
*/
get_header();
?>
<div id="apply-now" class="bg-gray">
	<div class="wrapper bg-white txt-center">
		<div class="header-content">
			<?php the_content(); ?>
		</div>
		<form action="<?php echo get_template_directory_uri(); ?>/library/forms.php" method="POST" class="form">
			<input type="hidden" name="Title" value="Wine Consultation Form">
			<div class="single-input">
				<input type="text" placeholder="First Name *" name="First_name" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Your Last Name *" name="Last_Name" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Company Name" name=Company_Name>
			</div>
			<div class="single-input">
				<input type="Email" placeholder="Email *" name="Email" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Phone Number *" name="Phone_Number" required>
			</div>
			<div class="single-input">
				<textarea id="" cols="30" rows="10" placeholder="Tell us a little about what we can help you with?" name="Tell_us_how_we_can_Help"></textarea>
			</div>
			<div class="two-columns">
				<div class="col">
					<div class="input-icon">
						<input type="text" class="input" placeholder="Desired Date" name="Desired_Date" required>
						<a class="icon" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-date.png" alt=""></a>
					</div>
				</div>
				<div class="col">
					<div class="input-icon">
						<input type="text" class="input" placeholder="Desired Hour" name="Desired_Hour" required>
						<a class="icon" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-hour.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Locations of Event (Let us know if you would like recommendations.) *" name="Locations_of_Event" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Estimated Number of Guests *" name="Estimated_Guests" required>
			</div>
			<div class="txt-center">
				<a href="#" class="btn submit purple">SEND</a>
			</div>
		</form>
	</div>
</div>
<?php get_footer(); ?>