<?php
/*
	Template Name: Careers
*/
get_header();
?>
<div id="careers" class="bg-gray">

	<div id="hero-section">
		<div class="content" style="background-image:url(<?php the_field('hero_section', 17); ?>)">
			<div class="aligned">
				<?php the_field('hero_section_content', 17); ?>
			</div>
		</div>
	</div>

	<div class="wrapper">

		<div class="career-list group">
			<?php

			$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

			$args = array(
				'post_type'      => 'careers',
				'posts_per_page' => 5,
				'paged'          => get_query_var( 'paged' )
			);

			$query = new WP_Query( $args );
			$posts = $query->posts;
		?>

		<?php
			foreach($posts as $post):
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		?>
			<div class="career-post">
				<div class="content">
					<h2><?php the_title(); ?></h2>

					<div class="excerpt">
						<center>Behind the Scenes (BTS) Catering and Events is looking to add experienced and passionate people to our team!</center>
					</div>

					<div class="cta group txt-center">
						<a class="btn purple" href="<?php the_permalink(); ?>">Full Description</a>
						<a class="btn brown" href="<?php the_field('form_link', 17); ?>">Apply</a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
		</div>

	</div>
	<!-- end wrapper -->

</div>
<?php get_footer(); ?>