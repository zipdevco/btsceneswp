<?php
/*
	Template Name: Home Page
*/
get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div id="template-home">

		<?php get_template_part( 'template-parts/hero-section' ); ?>

		<section id="philosophy">
			<div class="wrapper txt-center">
				<h1><?php the_field('philosophy_title'); ?></h1>
				<?php the_field('philosophy_text'); ?>
				<div class="quote">
					<?php the_field('philosophy_quote'); ?>
				</div>
			</div>
		</section>
		<section id="bottom-logos">
			<div class="logo-list">
				<div class="wrapper group">
					<?php
					if( have_rows('logos') ):
					    while ( have_rows('logos') ) : the_row();
					?>
						<div class="col" style="width:<?php the_sub_field('size'); ?>;">
							<a target="_blank" href="<?php the_sub_field('url');?>">
								<img src="<?php the_sub_field('logo'); ?>" alt="">
							</a>
						</div>
					<?php
					    endwhile;
					endif;
					?>
				</div>
				<!-- end wrapper -->
			</div>
		</section>
		<!-- end bottom-logos -->

	</div>
	<!-- template-home -->

	<?php endwhile; ?>

<?php endif;?>

<?php get_footer(); ?>