<?php
/*
	Template Name: Request Proposal
*/
get_header();
?>
<div id="apply-now" class="bg-gray">
	<div class="wrapper bg-white txt-center">
		<div class="header-content">
			<?php the_content(); ?>
		</div>
		<form action="<?php echo get_template_directory_uri(); ?>/library/forms.php" method="POST" class="form">
			<input type="hidden" name="Title" value="Proposal Form">
			<div class="single-input">
				<input type="text" placeholder="First Name *" name="First_Name" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Your Last Name *" name="Last_Name" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Company Name" name="Company_Name">
			</div>
			<div class="single-input">
				<input type="email" placeholder="Email *" name="Email" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Phone Number *" name="Phone_Number" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="What can we help you with? *" name="What_Can_We_Help" required>
			</div>
			<div class="single-input">
				<input type="text" placeholder="How did you hear about us ? *" name="How_Did_You_Hear" required>
			</div>
			<div class="two-columns">
				<div class="col">
					<div class="input-icon">
						<input type="text" class="input datepicker" placeholder="Desired Date" name="Desired_Date" required>
						<a class="icon" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-date.png" alt=""></a>
					</div>
				</div>
				<div class="col">
					<div class="input-icon">
						<input type="text" class="input timepicker" placeholder="Desired Hour" name="Desired_Hour" required>
						<a class="icon" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/ico-hour.png" alt=""></a>
					</div>
				</div>
			</div>
			<div class="single-input">
				<input type="text" placeholder="Estimated number of guests? *" name="Estimated_Number_of_Guests" required>
			</div>
			<textarea name="" id="" cols="30" rows="10" placeholder="Tell us a little about your event. (Style, type, location, theme, etc.)" name="Tell_Us_a_little_Event"></textarea>
			<div class="txt-center">
				<a href="#" class="submit btn purple">SEND</a>
			</div>
		</form>
	</div>
</div>
<?php get_footer(); ?>