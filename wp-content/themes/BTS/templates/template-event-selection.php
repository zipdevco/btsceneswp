<?php
/*
	Template name: Event Selection
*/
get_header();
?>

<div id="event-selection">

	<?php if( have_rows('events') ):	?>

	<section id="hero-section">

		<div class="slider">

			<?php

				$count = 1;

				while ( have_rows('events') ) : the_row();

				$active    = ($count == 1) ? 'active' : '';
				$postID    = get_sub_field('select_event');
				$title     = get_the_title($postID);
				$content   = get_post_field('post_content', $postID);
				$permalink = get_field('checkout_page') . '?id=' . $postID;
			?>

				<div id="slider-<?php echo $count; ?>" class="slide animated <?php echo $active; ?>">
					<div class="content" style="background-image:url('<?php the_sub_field('background_image'); ?>');">
						<div class="aligned">
							<div class="wrapper txt-center">
								<h1><?php echo $title; ?></h1>
								<?php echo do_shortcode( $content ); ?>
								<div class="cta">
									<a href="<?php echo $permalink; ?>" class="btn brown">BUY TICKETS</a>
								</div>
							</div>
						</div>
					</div>
				</div>

			<?php
				$count++;

				endwhile;
			?>

			<ul class="slider-control">

				<?php

					$count = 1;

					while ( have_rows('events') ) : the_row();

					$active = ($count == 1) ? 'active' : '';
				?>

					<li class="<?php echo $active; ?>"><a href="#slider-<?php echo $count; ?>"></a></li>

				<?php
					$count++;

					endwhile;
				?>

			</ul>

		</div>

	</section>
	<!-- end hero section -->

	<?php endif; ?>

</div>
<!-- end checkout page -->

<?php get_footer(); ?>