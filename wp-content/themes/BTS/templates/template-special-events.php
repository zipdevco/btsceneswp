<?php
/*
	Template Name: Special Events
*/
get_header();
?>

<div id="special-events">

	<?php get_template_part( 'template-parts/hero-section' ); ?>

	<section id="special-v-1">
		<div class="wrapper">
			<div class="text">
				<?php the_field('events_text'); ?>
			</div>
			<div class="four-columns">
				<?php
					// check if the repeater field has rows of data
				if( have_rows('category_columns') ):

				 	// loop through the rows of data
				    while ( have_rows('category_columns') ) : the_row();
				?>

					<div class="col txt-center">
						<img src="<?php the_sub_field('category_icon'); ?>" alt="">
						<h3><?php the_sub_field('category_title'); ?></h3>
						<p><?php the_sub_field('category_text'); ?></p>
					</div>

				<?php
				    endwhile;

				endif;
				?>
			</div>
		</div>
	</section>
	<!-- end special-v-1 -->

	<section id="special-v-2" style="background-image:url(<?php the_field('wed_bg'); ?>);">
		<div class="wrapper">
			<?php the_field('wed_content'); ?>
			<div class="three-columns">

				<?php
				if( have_rows('wed_categories') ):
				    while ( have_rows('wed_categories') ) : the_row();
				?>

					<div class="col">
						<img src="<?php the_sub_field('wed_icon'); ?>" alt="">
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
					</div>

				<?php
				    endwhile;
				endif;
				?>

			</div>
			<!-- end three-columns -->
		</div>
		<!-- end wrapper -->
	</section>
	<!-- end special-v-2-->

	<section id="special-v-3" style="background-image:url(<?php the_field('so_background'); ?>);">
		<div class="wrapper">
			<h2><?php the_field('so_title'); ?></h2>
			<p class="quote"><?php the_field('so_text'); ?></p>
			<a class="btn brown" href="<?php the_field('so_cta_link'); ?>"><?php the_field('so_cta_text'); ?></a>
		</div>
		<!-- page-wrapper -->
	</section>
	<!-- end special-v-3 -->

	<section id="special-v-4">
		<div class="wrapper">
			<?php the_field('beli_content'); ?>
			<div class="three-columns">
				<?php
				if( have_rows('bel_categories') ):

				    while ( have_rows('bel_categories') ) : the_row();
				?>

					<div class="col">
						<img src="<?php the_sub_field('wed_icon'); ?>" alt="">
						<h3><?php the_sub_field('title'); ?></h3>
						<p><?php the_sub_field('description'); ?></p>
					</div>

				<?php
				    endwhile;

				endif;
				?>
			</div>
			<!-- end three-column -->
		</div>
		<!-- end wrapper -->
	</section>
	<!-- end special-v-4-->

	<section id="special-v-8" style="background-image:url(<?php the_field('cta_background'); ?>);">
		<div class="wrapper">
			<?php the_field('cta_content'); ?>
			<a class="btn brown" href="<?php the_field('cta_cta_link'); ?>"><?php the_field('cta_cta_text'); ?></a>
		</div>
		<!-- page-wrapper -->
	</section>
	<!-- end special-v-8 -->

	<section id="special-v-5">
		<div class="slider-container">
			<a id="left-col" class="control active" href="#left"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/back-arrow.png" alt=""></a>
			<a id="right-col" class="control" href="#right"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/next-arrow.png" alt=""></a>
			<div class="columns group" data-state="0">
				<?php
				if( have_rows('venues') ):

				    while ( have_rows('venues') ) : the_row();
				?>

					<div class="col txt-center" style="background-image:url(<?php the_sub_field('background_image'); ?>);">
						
						<div class="overlay">
							<h3><?php the_sub_field('venue_name'); ?></h3>
							<p>
								<span>Location: <?php the_sub_field('location'); ?></span>
								<span>Type of Event: <?php the_sub_field('type_of_event'); ?></span>
								<span><?php the_sub_field('attendees'); ?></span>
								<span><?php the_sub_field('website_text'); ?></span>
		
							</p>
						</div>
					</div>

				<?php
				    endwhile;
				endif;
				?>
			</div>
			<!-- end three-columns -->
		</div>
		<!-- end wrapper -->
	</section>
	<!-- end special-v-5 -->

	<section id="special-v-7" style="background-image:url(<?php the_field('bottom_background'); ?>);">
		<div class="wrapper txt-center">
			<?php the_field('bottom_content'); ?>
			<div class="three-columns">
				<?php
				if( have_rows('bottom_cols') ):

				    while ( have_rows('bottom_cols') ) : the_row();
				?>

					<div class="col">
						<img src="<?php the_sub_field('icon'); ?>" alt="">
						<h3><?php the_sub_field('title'); ?></h3>
					</div>

				<?php
				    endwhile;

				endif;
				?>
			</div>
			<!-- end three-columns -->
		</div>
		<!-- end wrapper-->
	</section>
	<!-- end special-v-7 -->

</div>
<!-- end special-events -->

<?php get_footer(); ?>