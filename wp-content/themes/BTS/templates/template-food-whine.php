<?php
/*
	Template Name: Food and Whine
*/
get_header();
?>

<div id="food-whine">

	<?php get_template_part( 'template-parts/hero-section' ); ?>

	<section id="fw-1">
		<div class="wrapper txt-center">
			<?php if( have_rows('fw_events') ):	?>

				<div class="three-columns">

					<?php
						while ( have_rows('fw_events') ) : the_row();
					?>

						<div class="col" style="background-image:url(<?php the_sub_field('background_image'); ?>);">
							<a class="linkbox overlay" href="<?php the_sub_field('link'); ?>">
								<p><?php the_sub_field('title'); ?></p>
							</a>
						</div>

					<?php
						endwhile;
					?>

				</div>

			<?php endif; ?>
		</div>
	</section>
	<!--end fw-1-->

</div>

<?php get_footer(); ?>