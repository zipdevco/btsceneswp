<?php
/*
	Template Name: Contact Us
*/
get_header();
?>

<div id="contact-page">

	<section id="contact" class="wrapper bg-white">
		<div class="contact-container">
			<h1>CONTACT US</h1>
			<form action="<?php echo get_template_directory_uri(); ?>/library/forms.php" method="POST" class="form">
				<input type="hidden" name="Title" value="Contact Us Form">
				<div class="input gray">
					<input type="text" placeholder="Name *" name="Name" required>
				</div>
				<div class="input gray">
					<input type="email" placeholder="Email *" name="Email" required>
				</div>
				<div class="input gray">
					<textarea id="" cols="30" rows="10" placeholder="Message *" name="Message" required></textarea>
				</div>
				<div class="input">
					<a href="#" class="submit btn brown">SEND</a>
				</div>
			</form>
		</div>
	</section>
	<!-- end contact -->

	<section id="mailing" class="wrapper bg-gray">
		<div class="mail-container">
			<div class='mailmunch-forms-widget-505787'></div>
		</div>
	</section>
	<!-- end mailing -->

</div>

<?php get_footer(); ?>