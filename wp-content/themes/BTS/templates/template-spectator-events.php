<?php
/*
	Template Name: Spectator Events
*/
get_header();
?>

<div id="spectator-events">

	<?php get_template_part( 'template-parts/hero-section' ); ?>

	<section id="sv-1">
		<div class="wrapper">
			<?php the_field('media_content'); ?>
		</div>
	</section>
	<!-- end sv-1 -->

	<section id="sv-2" class="bg-gray">

		<div class="wrapper txt-center">

			<div class="header-container">
				<?php the_field('sporting_events_content'); ?>
			</div>

			<?php if( have_rows('sport_events') ):	?>

				<div class="four-columns">

					<?php
						while ( have_rows('sport_events') ) : the_row();
					?>

						<div class="col" style="background-image:url(<?php the_sub_field('background_image'); ?>);">
							<a class="linkbox overlay" href="<?php the_sub_field('link'); ?>">
								<p><?php the_sub_field('title'); ?></p>
							</a>
						</div>

					<?php
						endwhile;
					?>

				</div>

			<?php endif; ?>

			<div class="cta-container">
				<a class="btn brown" href="<?php the_field('cta_link'); ?>"><?php the_field('cta_text'); ?></a>
			</div>

		</div>
		<!-- end wrapper -->

	</section>
	<!-- end sv-2-->

</div>

<?php get_footer(); ?>