<?php
/*
	Template Name: Our Story
*/
get_header();
?>

<div id="our-story">

	<section id="hero-section">
		<div class="content" style="background-image:url(<?php the_field('bg_image'); ?>);">
			<div class="aligned">
				<?php the_field('hero_content'); ?>
			</div>
		</div>
	</section>

	<section id="s-os-1" class="bg-gray">
		<div class="wrapper">
			<?php the_field('gray_section_content'); ?>
		</div>
		<!-- end wrapper -->
	</section>
	<!-- end s-os-1 -->

	<section id="s-os-2">
		<div class="wrapper">
			<div class="team-container">

				<div class="header-container">
					<?php the_field('who_content'); ?>
				</div>

				<div class="alternate-horiz">
					<?php if( have_rows('employees') ):	?>

						<?php while ( have_rows('employees') ) : the_row(); ?>

							<div class="row group">
								<div class="image">
									<img src="<?php the_sub_field('picture'); ?>" alt="">
								</div>
								<div class="content">
									<h3><?php the_sub_field('name'); ?></h3>
									<p><?php the_sub_field('position'); ?></p>
									<div class="bio">
										<?php the_sub_field('biography'); ?>
									</div>
								</div>
							</div>

						<?php endwhile;	?>

					<?php endif; ?>
				</div>
				<!-- end alternate-horiz -->

			</div>
			<!-- end team-container -->
		</div>
		<!-- end wrapper -->
	</section>
	<!-- end s-os-2 -->

</div>

<?php get_footer(); ?>