<?php
/*
	Template Name: Checkout
*/
get_header();
?>

<div id="checkout" class="bg-gray">

	<div class="wrapper bg-white">

		<h1 class="txt-center"><?php the_title(); ?></h1>

		<nav id="step-nav" class="group">
			<ul>
				<li <?php if(!isset($_GET['event'])): echo 'class="active"'; endif; ?>><a href="/checkout/">Choose Show</a></li>
				<li <?php if($_GET['step'] == 2): echo 'class="active"'; endif; ?>><a href="#step-2">Menus</a></li>
				<li><a href="#step-3">Payment Information</a></li>
			</ul>
		</nav>

		<div class="messages"></div>

		<?php if(!isset($_GET['event'])):?>

		<section id="step-1">

			<?php

				$startday = date('Y/m/d', strtotime('-1 day'));
				$endday   = date('Y/m/d', strtotime('+1 year'));

				$args = array(
					'post_type'      => 'events',
					'posts_per_page' => -1,
					'orderby'        => 'meta_value',
					'meta_key'       => 'date',
					'order'          => 'ASC',
					'meta_query'     =>
					array(
						array
					    (
					        'key'     => 'date',
					        'value'   => array( $startday, $endday ),
					        'compare' => 'BETWEEN',
					        'type'    => 'DATE'
					    )
					)
				);

				$query = new WP_Query($args);
				$posts = $query->posts;
			?>

			<div class="txt-center">
				<form action="/checkout/" method="GET" class="form-checkout">
					<input type="hidden" name="step" value="2">
					<div class="row select">
						<select name="event">
							<?php

								foreach($posts as $post):

									$selected = ($post->ID == $_GET['id']) ? 'selected="selected"' : '';

							?>
								<option value="<?php echo $post->ID; ?>" <?php echo $selected; ?>><?php echo get_the_title($post->ID) . ' @ ' . get_field('date'); ?></option>
							<?php endforeach;?>
						</select>
					</div>
					<div class="row">
						<a class="btn brown submit" href="#">NEXT</a>
					</div>
				</form>
			</div>

		</section>
		<!-- end step-1 -->

		<?php endif;  ?>

		<section id="step-2">

			<form id="menu-checkout" action="/checkout/" method="GET" class="form-checkout group">

				<div class="content">
						<?php

						if(isset($_GET['event'])):

							//Get Menu
							$id     = $_GET['event'];
							$menus  = get_field('vip_menu', $id);

							foreach ($menus as $menu):

								// check if the repeater field has rows of data
								if( have_rows('dish', $menu->ID) ):

								 	// loop through the rows of data
								    while ( have_rows('dish', $menu->ID) ) : the_row();

										echo '<h2>' . get_sub_field('category'). '</h2>';

										while ( have_rows('dish') ) : the_row();
					?>

					<div class="row group">
						<div class="dish-data">

							<?php $maindish =  get_sub_field('dish_name'); ?>
							<label>
								<span><?php echo $maindish; ?></span>
								<?php if(get_sub_field('dish_description')): ?>
								<span class="description"><?php the_sub_field('dish_description'); ?></span>
								<?php endif; ?>
							</label>
						</div>
						<div class="form-data">
							<?php if(get_sub_field('price')): ?>
							<label>
								<input type="checkbox" name="<?php the_sub_field('dish_name'); ?>" value="<?php the_sub_field('price'); ?>">
								$<?php the_sub_field('price'); ?>
							</label>
							<?php endif; ?>
						</div>
					</div>

					<?php if( have_rows('variations') ): ?>
					<div class="variations">
						<?php while ( have_rows('variations') ) : the_row(); ?>
						<div class="row group variation">
							<div class="dish-data">
								<label><span><?php the_sub_field('variation_title'); ?></span></label>
							</div>
							<div class="form-data">
								<?php if(get_sub_field('variation_price')): ?>
								<label>
									<input type="checkbox" data-variation="<?php echo $maindish; ?>" name="<?php the_sub_field('variation_title'); ?>" value="<?php the_sub_field('variation_price'); ?>">
									$<?php the_sub_field('variation_price'); ?>
								</label>
								<?php endif; ?>
							</div>
						</div>
						<?php endwhile; ?>

					</div>

					<?php endif; ?>

					<?php

										endwhile;

								    endwhile;

								endif;


							endforeach;

					?>

				</div>
				<!-- end content -->

				<div class="sidebar">
					<div class="sticky">
						<div class="total">
							<p>Subtotal: <span class="getsubtotal">$ 0.00</span></p>
							<p>Service Charge (<?php the_field('service_charge', 'options'); ?>%): <span class="getcharge">$ 0.00</span></p>
							<p>Tax (<?php the_field('tax_charge', 'options'); ?>%): <span class="gettax">$ 0.00</span></p>
							<p>Total: <span class="gettotal">$ 0.00</span></p>
						</div>
						<input type="hidden" id="payerID" value="" />
						<input type="hidden" id="paymentID" value="" />
						<input type="hidden" id="charge-data" value="<?php the_field('service_charge', 'options'); ?>" />
						<input type="hidden" id="tax-data" value="<?php the_field('tax_charge', 'options'); ?>" />
						<input type="hidden" id="orderID" value="<?php echo uniqid(); ?>" />

						<input type="hidden" id="subtotal-checkout" value="0.00" />
						<input type="hidden" id="charge-checkout" value="0.00" />
						<input type="hidden" id="tax-checkout" value="0.00" />
						<input type="hidden" id="total-checkout" value="0.00" />
						<a id="proceed" href="#step-3" class="btn purple">NEXT</a>
					</div>
				</div>
				<!-- end sidebar -->

			</form>

		</section>
		<!-- end step-2 -->

		<?php endif;  ?>

		<section id="step-3">

			<div class="final-checkout group">

				<div id="checkout-data">

					<h2>Order Summary</h2>
					<table>
						<thead>
							<tr>
								<td>Item Description</td>
								<td>Price</td>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr><td>Subtotal</td><td><span class="getsubtotal">$ 0.00</span></td></tr>
							<tr><td>Service Charge (<?php the_field('service_charge', 'options'); ?>%):</td><td><span class="getcharge">$ 0.00</span></td></tr>
							<tr><td>Tax (<?php the_field('tax_charge', 'options'); ?>%):</td><td><span class="gettax">$ 0.00</span></td></tr>
							<tr><td>Total</td><td><span class="gettotal">$ 0.00</span></td></tr>
						</tfoot>
					</table>

				</div>
				<!-- end checkout-data -->

				<div class="payment">

					<div id="customer-information">

						<h2>Customer Information</h2>
						<form id="client-information">
							<div class="form-row">
								<label>Name*</label>
								<input id="data-name" name="Name" type="text" required>
							</div>
							<div class="form-row">
								<label>Phone Number*</label>
								<input id="data-phone" name="Phone" type="text" required>
							</div>
							<div class="form-row">
								<label>Email*</label>
								<input id="data-email" name="Email" type="email" required>
							</div>
							<div class="form-row">
								<label>Table Letter*</label>
								<input id="data-tableletter" name="TableLetter" type="text" required>
							</div>
							<div class="form-row">
								<label>Table Number*</label>
								<input id="data-tablenumber" name="TableNumber" type="number" required>
							</div>
							<div class="form-row">
								<label>Estimated Time of Arrival*</label>
								<input id="data-timearrival" class="timepicker" name="TimeofArrival" type="text" required>
							</div>
						</form>

						<h2>Proceed with Payment</h2>
						<div id="paypal-button-container"><div id="paypal-submit-overlay" class="disable"></div></div>
						<a class="invisible" id="finalize" href="#">Finalize</a>

					</div>

				</div>
			</div>

		</section>
		<!-- end step-3 -->

		<section id="step-4">
			<h2 class="txt-center">Thank you for your purchase, we will be in touch with you.</h2>
		</section>

	</div>

</div>
<!-- end checkout -->

<?php get_footer(); ?>