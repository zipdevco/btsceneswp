<?php
/*
	Template Name: News
*/
get_header();
?>
<div id="news" class="bg-gray">
	<div class="wrapper bg-white">
		<section id="news-container">
			<h1><?php the_title(); ?></h1>

			<?php

				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

				$args = array(
					'posts_per_page' => 5,
					'paged'          => get_query_var( 'paged' )
				);

				$query = new WP_Query( $args );
				$posts = $query->posts;
			?>

			<?php
				foreach($posts as $post):
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
			?>
				<div class="post">
					<div class="image" style="background-image: url(<?php echo $image[0]; ?>);"></div>
					<div class="excerpt">
						<h2><?php the_title(); ?></h2>
						<p>
							<?php the_excerpt(); ?>
						</p>
						<div class="txt-right">
							<a class="txt-center btn brown-outline" href="<?php the_permalink(); ?>">Read Full Article</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</section>
	</div>
</div>
<?php get_footer(); ?>