<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png" type="image/png"/>
	<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png" type="image/png"/>

	<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<header id="main-header">
	<div class="wrapper group">
		<div id="logo">
			<a href="<?php echo site_url(); ?>">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bts-header-logo.png" alt="">
			</a>
		</div>
		<nav id="main-navigation" class="group">
			<?php wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' => '') ); ?>
		</nav>
		<a href="#" id="mobile-button">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</a>
	</div>
</header>
<nav id="mobile-navigation" class="">
	<?php wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' => '') ); ?>
</nav>
<!-- end main-header -->