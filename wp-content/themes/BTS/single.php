<?php get_header(); ?>

	<div class="single-post bg-gray">
		<div class="wrapper bg-white">

			<div class="post">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<?php the_content(); ?>

					<?php endwhile; ?>

				<?php endif;?>
			</div>

		</div>
	</div>

<?php get_footer(); ?>