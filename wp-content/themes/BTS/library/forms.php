<?php
/*
	PHPmailerINIT
*/
require_once('phpmailer/PHPMailerAutoload.php');
/*
	WordpressINIT
*/
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

function sendMail(){

	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->Host       = "localhost";
	$mail->Port       = 25;

	$mail->setFrom('noreply@btsscenes.com', 'BTS');
	$mail->addAddress(get_field('form_receiver', 'options'));
	$mail->Subject = $_POST['Title'];

	$message .= '<table><tbody>';

	foreach ($_POST as $key => $value):

		if(is_array($value)):

			foreach ($value as $key2 => $value2):

				if(is_array($value2)):

					foreach ($value2 as $key3 => $value3):

						$message .= '<tr><td>' . $key3 . '</td><td>' . $value3 . '<td></tr>';

					endforeach;

				else:

					$message .= '<tr><td>' . $key2 . '</td><td>' . $value2 . '<td></tr>';

				endif;


			endforeach;

		else:

			$message .= '<tr><td>' . $key . '</td><td>' . $value . '<td></tr>';

		endif;

	endforeach;

	$message .= '</tbody></table>';

	foreach ($_POST as $key => $value):
		$altmessage .= $key .' : ' .$value . PHP_EOL;
	endforeach;

	$mail->Body = $message;
	$mail->AltBody = $altmessage;
	$mail->isHTML(true);

	if(!$mail->send()):
		$output['mail_status'] = false;
	else:
		$output['mail_status'] = true;
	endif;

	$file    = get_template_directory() . '/mails/log.log';
	$current = file_get_contents($file);
	$date    = date("D M d, Y G:i");

	$current .= '['.$date.']' . PHP_EOL;
	$current .= $altmessage . PHP_EOL;

	$write = file_put_contents($file, $current);

	return $output;
}


$output = sendMail();
header('Content-Type: application/json');
echo json_encode($output);
?>