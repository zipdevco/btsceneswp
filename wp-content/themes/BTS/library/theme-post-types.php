<?php
  function custom_post_type()
  {

    $testimonials_labels = array(
      'name'                  => _x( 'Testimonials', 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'text_domain' ),
      'menu_name'             => __( 'Testimonials', 'text_domain' ),
      'name_admin_bar'        => __( 'Testimonials', 'text_domain' ),
      'archives'              => __( 'Item Archives', 'text_domain' ),
      'attributes'            => __( 'Item Attributes', 'text_domain' ),
      'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
      'all_items'             => __( 'All Testimonials', 'text_domain' ),
      'add_new_item'          => __( 'Add New Testimonial', 'text_domain' ),
      'add_new'               => __( 'Add New Testimonial', 'text_domain' ),
      'new_item'              => __( 'New Item', 'text_domain' ),
      'edit_item'             => __( 'Edit Item', 'text_domain' ),
      'update_item'           => __( 'Update Item', 'text_domain' ),
      'view_item'             => __( 'View Item', 'text_domain' ),
      'view_items'            => __( 'View Items', 'text_domain' ),
      'search_items'          => __( 'Search Item', 'text_domain' ),
      'not_found'             => __( 'Not found', 'text_domain' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
      'featured_image'        => __( 'Featured Image', 'text_domain' ),
      'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
      'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
      'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
      'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
      'items_list'            => __( 'Items list', 'text_domain' ),
      'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
      'filter_items_list'     => __( 'Filter items list', 'text_domain' )
    );

    $testimonials_args = array(
      'label'                 => __( 'Testimonial', 'text_domain' ),
      'description'           => __( 'Testimonials', 'text_domain' ),
      'labels'                => $testimonials_labels,
      'supports'              => array( ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 5,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );

    $careers_labels = array(
      'name'                  => _x( 'Careers', 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( 'Career', 'Post Type Singular Name', 'text_domain' ),
      'menu_name'             => __( 'Careers', 'text_domain' ),
      'name_admin_bar'        => __( 'Careers', 'text_domain' ),
      'archives'              => __( 'Item Archives', 'text_domain' ),
      'attributes'            => __( 'Item Attributes', 'text_domain' ),
      'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
      'all_items'             => __( 'All Careers', 'text_domain' ),
      'add_new_item'          => __( 'Add New Career', 'text_domain' ),
      'add_new'               => __( 'Add New Career', 'text_domain' ),
      'new_item'              => __( 'New Item', 'text_domain' ),
      'edit_item'             => __( 'Edit Item', 'text_domain' ),
      'update_item'           => __( 'Update Item', 'text_domain' ),
      'view_item'             => __( 'View Item', 'text_domain' ),
      'view_items'            => __( 'View Items', 'text_domain' ),
      'search_items'          => __( 'Search Item', 'text_domain' ),
      'not_found'             => __( 'Not found', 'text_domain' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
      'featured_image'        => __( 'Featured Image', 'text_domain' ),
      'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
      'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
      'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
      'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
      'items_list'            => __( 'Items list', 'text_domain' ),
      'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
      'filter_items_list'     => __( 'Filter items list', 'text_domain' )
    );

    $careers_args = array(
      'label'                 => __( 'Career', 'text_domain' ),
      'description'           => __( 'Careers', 'text_domain' ),
      'labels'                => $careers_labels,
      'supports'              => array( ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 6,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );

    $menus_labels = array(
      'name'                  => _x( 'Menus', 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( 'Menu', 'Post Type Singular Name', 'text_domain' ),
      'menu_name'             => __( 'Menus', 'text_domain' ),
      'name_admin_bar'        => __( 'Menus', 'text_domain' ),
      'archives'              => __( 'Item Archives', 'text_domain' ),
      'attributes'            => __( 'Item Attributes', 'text_domain' ),
      'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
      'all_items'             => __( 'All Menus', 'text_domain' ),
      'add_new_item'          => __( 'Add New Menu', 'text_domain' ),
      'add_new'               => __( 'Add New Menu', 'text_domain' ),
      'new_item'              => __( 'New Item', 'text_domain' ),
      'edit_item'             => __( 'Edit Item', 'text_domain' ),
      'update_item'           => __( 'Update Item', 'text_domain' ),
      'view_item'             => __( 'View Item', 'text_domain' ),
      'view_items'            => __( 'View Items', 'text_domain' ),
      'search_items'          => __( 'Search Item', 'text_domain' ),
      'not_found'             => __( 'Not found', 'text_domain' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
      'featured_image'        => __( 'Featured Image', 'text_domain' ),
      'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
      'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
      'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
      'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
      'items_list'            => __( 'Items list', 'text_domain' ),
      'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
      'filter_items_list'     => __( 'Filter items list', 'text_domain' )
    );

    $menus_args = array(
      'label'                 => __( 'Menu', 'text_domain' ),
      'description'           => __( 'Menus', 'text_domain' ),
      'labels'                => $menus_labels,
      'supports'              => array( ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_position'         => 7,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );

    $events_labels = array(
      'name'                  => _x( 'Events', 'Post Type General Name', 'text_domain' ),
      'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'text_domain' ),
      'Event_name'            => __( 'Events', 'text_domain' ),
      'name_admin_bar'        => __( 'Events', 'text_domain' ),
      'archives'              => __( 'Item Archives', 'text_domain' ),
      'attributes'            => __( 'Item Attributes', 'text_domain' ),
      'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
      'all_items'             => __( 'All Events', 'text_domain' ),
      'add_new_item'          => __( 'Add New Event', 'text_domain' ),
      'add_new'               => __( 'Add New Event', 'text_domain' ),
      'new_item'              => __( 'New Item', 'text_domain' ),
      'edit_item'             => __( 'Edit Item', 'text_domain' ),
      'update_item'           => __( 'Update Item', 'text_domain' ),
      'view_item'             => __( 'View Item', 'text_domain' ),
      'view_items'            => __( 'View Items', 'text_domain' ),
      'search_items'          => __( 'Search Item', 'text_domain' ),
      'not_found'             => __( 'Not found', 'text_domain' ),
      'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
      'featured_image'        => __( 'Featured Image', 'text_domain' ),
      'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
      'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
      'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
      'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
      'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
      'items_list'            => __( 'Items list', 'text_domain' ),
      'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
      'filter_items_list'     => __( 'Filter items list', 'text_domain' )
    );

    $events_args = array(
      'label'                 => __( 'Event', 'text_domain' ),
      'description'           => __( 'Events', 'text_domain' ),
      'labels'                => $events_labels,
      'supports'              => array( ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_Event'         => true,
      'Event_position'        => 8,
      'show_in_admin_bar'     => true,
      'show_in_nav_Events'    => true,
      'can_export'            => true,
      'has_archive'           => true,
      'exclude_from_search'   => false,
      'publicly_queryable'    => true,
      'capability_type'       => 'page',
    );

    register_post_type( 'testimonials', $testimonials_args );
    register_post_type( 'careers'     , $careers_args );
    register_post_type( 'menus'       , $menus_args );
    register_post_type( 'events'      , $events_args );

  }

add_action( 'init', 'custom_post_type', 0 );
?>
