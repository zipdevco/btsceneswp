<?php

function theme_scripts()
{

	// Enqueue the main Stylesheet.
	wp_enqueue_style( 'theme', get_template_directory_uri() . '/assets/css/main.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'picker-css', get_template_directory_uri() . '/assets/css/default.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'picker.d-css', get_template_directory_uri() . '/assets/css/default.date.css', array(), '1.0.0', 'all' );
	wp_enqueue_style( 'picker.t-css', get_template_directory_uri() . '/assets/css/default.time.css', array(), '1.0.0', 'all' );

	// Deregister the jquery version bundled with WordPress.
	wp_deregister_script( 'jquery' );

	// CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js', array(), '2.1.0', false );
	wp_enqueue_script( 'j-validate',  get_template_directory_uri() . '/assets/js/jquery.validate.min.js', array('jquery'), '2.1.0', true );
	wp_enqueue_script( 'momentjs',  get_template_directory_uri() . '/assets/js/moment-with-locales.min.js', array('jquery'), '2.1.0', true );
	wp_enqueue_script( 'picker',  get_template_directory_uri() . '/assets/js/picker.js', array('jquery'), '2.1.0', true );
	wp_enqueue_script( 'picker-d',  get_template_directory_uri() . '/assets/js/picker.date.js', array('jquery'), '2.1.0', true );
	wp_enqueue_script( 'picker-t',  get_template_directory_uri() . '/assets/js/picker.time.js', array('jquery'), '2.1.0', true );

	//Main JS
	wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'slider', get_template_directory_uri() . '/assets/js/slider.js', array('jquery'), '1.0.0', true );

	if(is_page_template('templates/template-checkout.php')):
		wp_enqueue_script( 'paypal', get_template_directory_uri() . '/assets/js/paypal.js', array('jquery'), '1.0.0', true );
	endif;
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' );