<?php get_header(); ?>
<div id="careers" class="bg-gray">

	<div class="wrapper">

		<div class="career-single white-bg">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<h1><?php the_title(); ?></h1>

				<?php the_content(); ?>

			<?php endwhile; ?>

		<?php endif;?>

			<div class="cta center">
				<a class="btn purple" href="<?php the_field('form_link', 17); ?>">Apply</a>
			</div>

		</div>

	</div>
	<!-- end wrapper -->

</div>
<?php get_footer(); ?>